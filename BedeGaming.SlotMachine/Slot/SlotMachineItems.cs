﻿namespace BedeGaming.SlotMachine.Slot
{
    public class SlotMachineItems<T>
    {
        public double Probability { get; set; }
        public T Item { get; set; }
    }
}
