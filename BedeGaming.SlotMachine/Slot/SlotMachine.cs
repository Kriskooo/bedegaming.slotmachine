﻿using BedeGaming.SlotMachine.CapitalHandlers;

namespace BedeGaming.SlotMachine
{
    public static class SlotMachine
    {

        public static void Run()
        {
            var capitalHandler = new CapitalHandler();
            var capital = capitalHandler.GetInitialBalance();

            capitalHandler.Gamble(capital);
        }
    }
}
